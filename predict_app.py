import os, random
import base64
import numpy as np
import tensorflow as tf

import io
from PIL import Image
from keras.models import load_model
from flask import request
from flask import Flask
from keras.preprocessing import image

import json

app = Flask(__name__)

print()
def get_model():
    global classifier
    global graph
    classifier = load_model('/Users/floris wieringa/Documents/coding/school/iframe/cnn/flower-classifier/my_model_flowers.h5')
    graph = tf.get_default_graph()

    print(" * Model loaded!")

def checkresult(result):
    if(result[0][0] == 1.0):
        return("Daisy")
    elif(result[0][1] == 1.0):
        return("Dandelion")
    elif(result[0][2] == 1.0):
        return("Rose")
    elif(result[0][3] == 1.0):
        return("Sunflower")
    elif(result[0][4] == 1.0):
        return("Tulip")
    else:
        return("I don't know what this is")

def randomImg(dir):
    imgList = [dir]
    for i in range(3):
        randImg = random.choice(os.listdir("/Users/floris wieringa/Documents/coding/school/iframe/cnn/flower-classifier/flowers_training/" + dir)) 
        imgList.append(randImg)
    print(imgList)
    return imgList


print(" * Loading Keras model...")
get_model()


@app.route("/predict", methods=["POST"])
def predict():
    message = request.get_json(force=True)
    encoded = message['image']

    msg = base64.b64decode(encoded)
    buf = io.BytesIO(msg)
    img = Image.open(buf)
    img = img.convert('RGB')
    img = img.resize((64, 64))

    img = image.img_to_array(img)
    img = np.expand_dims(img, axis=0)

    with graph.as_default():
        prediction = classifier.predict(img)
        result = checkresult(prediction)
        returnList = randomImg(result)
        jsonList = json.dumps(returnList)
        print(jsonList)
        return jsonList