# This is a web classifier using Flask for the backend.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Installing


Requirements

```
Python not higher than 3.6
Tensorflow version 1.15
Keras
Flask
Numpy
Pillow
```



## Deployment

To run the app use these two commands:

Linux
```
    export FLASK_APP=predict_app.py
    flask run --host=0.0.0.0
```
windows
```
    py set FLASK_APP=predict_app.py
    flask run --host=127.0.0.1
```