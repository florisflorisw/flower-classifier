
$( document ).ready(function() {

    var slideImg = ["work smarter not harder", "slideImg1", "slideImg2", "slideImg3"]; 

    function insertImages(list){
        $("#slideshow").css("opacity", "1"); 
        for (var i = 1; i < 4 ; i++) {
            console.log(list[i])
            $("#"+ slideImg[i]).attr("src","/static/flowers_test/"+list[0]+"/"+ list[i]);
        }
    }

    function insertText(flower){
        $("#prediction").text(flower);
        $("#h1-info").text("Some info about the: " + flower);
        switch(flower) {
            case "Rose":
                $("#flowerInfo").text("A rose is a woody perennial flowering plant of the genus Rosa, in the family Rosaceae, or the flower it bears. There are over three hundred species and thousands of cultivars. They form a group of plants that can be erect shrubs, climbing, or trailing, with stems that are often armed with sharp prickles. Flowers vary in size and shape and are usually large and showy, in colours ranging from white through yellows and reds. Most species are native to Asia, with smaller numbers native to Europe, North America, and northwestern Africa. Species, cultivars and hybrids are all widely grown for their beauty and often are fragrant. Roses have acquired cultural significance in many societies. Rose plants range in size from compact, miniature roses, to climbers that can reach seven meters in height. Different species hybridize easily, and this has been used in the development of the wide range of garden roses.[1]");
                break;
            case "Tulip":$
                $("#flowerInfo").text("Tulips (Tulipa) form a genus of spring-blooming perennial herbaceous bulbiferous geophytes (having bulbs as storage organs). The flowers are usually large, showy and brightly colored, generally red, pink, yellow, or white (usually in warm colors). They often have a different colored blotch at the base of the tepals (petals and sepals, collectively), internally. Because of a degree of variability within the populations, and a long history of cultivation, classification has been complex and controversial. The tulip is a member of the lily family, Liliaceae, along with 14 other genera, where it is most closely related to Amana, Erythronium and Gagea in the tribe Lilieae. There are about 75 species, and these are divided among four subgenera. The name is thought to be derived from a Persian word for turban, which it may have been thought to resemble. Tulips originally were found in a band stretching from Southern Europe to Central Asia, but since the seventeenth century have become widely naturalised and cultivated (see map). In their natural state they are adapted to steppes and mountainous areas with temperate climates. Flowering in the spring, they become dormant in the summer once the flowers and leaves die back, emerging above ground as a shoot from the underground bulb in early spring.")
                break;
            case "Daisy":$
                $("#flowerInfo").text("Bellis perennis is a common European species of daisy, of the family Asteraceae, often considered the archetypal species of that name. Many related plants also share the name daisy, so to distinguish this species from other daisies it is sometimes qualified as common daisy, lawn daisy or English daisy. Historically, it has also been commonly known as bruisewort and occasionally woundwort (although the common name woundwort is now more closely associated with Stachys). Bellis perennis is native to western, central and northern Europe, but widely naturalised in most temperate regions including the Americas[2][3] and Australasia. ")
                break;
            case "Sunflower":$
                $("#flowerInfo").text("Helianthus is a genus of plants comprising about 70 species.[4][5] Except for three species in South America, all Helianthus species are native to North America and Central America. The common names sunflower and common sunflower typically refer to the popular annual species Helianthus annuus, whose round flower heads in combination with the ligules look like the sun.[6] This and other species, notably Jerusalem artichoke (H. tuberosus), are cultivated in temperate regions and some tropical regions as food crops for humans, cattle, and poultry, and as ornamental plants.[7] The species H. annuus typically grows during the summer and into early fall, with the peak growth season being mid-summer.[8] ")
                break;
            case "Dandelion":$
                $("#flowerInfo").text("Taraxacum is a large genus of flowering plants in the family Asteraceae, which consists of species commonly known as dandelions. The genus is native to Eurasia and North America, but the two commonplace species worldwide, T. officinale and T. erythrospermum, were introduced from Europe and now propagate as wildflowers.[3] Both species are edible in their entirety.[4] The common name dandelion (/ˈdændɪlaɪ.ən/ DAN-di-ly-ən, from French dent-de-lion, meaning lion's tooth) is given to members of the genus. Like other members of the family Asteraceae, they have very small flowers collected together into a composite flower head. Each single flower in a head is called a floret. In part due to their abundance along with being a generalist species, dandelions are one of the most vital early spring nectar sources for a wide host of pollinators.[5] Many Taraxacum species produce seeds asexually by apomixis, where the seeds are produced without pollination, resulting in offspring that are genetically identical to the parent plant.")
                break;
          } 
    }

    console.log("test");
    let base64Image;
    $("#image-selector").change(function() {
        let reader = new window.FileReader();
        reader.onload = function(e) {
            let dataURL = reader.result;
            console.log(dataURL)
            $('#selected-image').attr("src", dataURL);
            base64Image = dataURL.replace("data:image/jpeg;base64,","");
        }
        reader.readAsDataURL($("#image-selector")[0].files[0]);
    });



    var i = Math.floor(Math.random() * 6) + 1;
    $("#my-div").load('/path-to-files/' + i + '.php');





    $("#predict-button").click(function(){
        let message = {
            image: base64Image
        }
        $.post("http://0.0.0.0:5000/predict", JSON.stringify(message), function(response){
            var responseList = JSON.parse(response);
            flower = responseList[0]
            insertText(flower);
            insertImages(responseList);
        });
        $.post("http://127.0.0.1:5000/predict", JSON.stringify(message), function(response){
            var responseList = JSON.parse(response);
            flower = responseList[0]
            insertText(flower);
            insertImages(responseList);
        });
    });
});